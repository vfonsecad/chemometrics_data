# Chemometrics Data

This repository contains datasets collected from several public archives

Each data folder contains:
- csv file with all the data found
- csv file with a data dictionary with the data documentation
- python file on how the csv files were created from raw data

The raw data is referenced inside the data dictionary and inside the python file
