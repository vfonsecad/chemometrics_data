#!/usr/bin/env python
# coding: utf-8

# In[2]:


import scipy.io as sp_io
import numpy as np
import pandas as pd
import os

data_name = "sugarcane"
# get raw data at https://data.mendeley.com/datasets/rnvvftvmh7/1
# all files
files = []
for file in os.walk("./raw/"):
    files.append(file)

files = sorted(files[0][2])
files


# In[50]:

# get ref data
ref_analyses = pd.read_csv("./raw/References.csv", sep = ";")
ref_analyses.iloc[11,1] = None #replace one "NA " value
ref_analyses["TS"] = ref_analyses["TS"].astype(np.float64)


# get data

F750 = pd.read_csv("./raw/F750.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in F750.columns[1:]]
F750.columns = cols_x
F750["instrument"] = "F750"
F750_final = pd.melt(F750, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")


Micronir1700rep1 = pd.read_csv("./raw/Micronir1700rep1.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Micronir1700rep1.columns[1:]]
Micronir1700rep1.columns = cols_x
Micronir1700rep1["instrument"] = "Micronir1700rep1"
Micronir1700rep1_final = pd.melt(Micronir1700rep1, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")

Micronir1700rep2 = pd.read_csv("./raw/Micronir1700rep2.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Micronir1700rep2.columns[1:]]
Micronir1700rep2.columns = cols_x
Micronir1700rep2["instrument"] = "Micronir1700rep2"
Micronir1700rep2_final = pd.melt(Micronir1700rep2, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")


Micronir1700rep3 = pd.read_csv("./raw/Micronir1700rep3.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Micronir1700rep3.columns[1:]]
Micronir1700rep3.columns = cols_x
Micronir1700rep3["instrument"] = "Micronir1700rep3"
Micronir1700rep3_final = pd.melt(Micronir1700rep3, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")


Micronir2200 = pd.read_csv("./raw/Micronir2200.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Micronir2200.columns[1:]]
Micronir2200.columns = cols_x
Micronir2200["instrument"] = "Micronir2200"
Micronir2200_final = pd.melt(Micronir2200, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")


Nirone22 = pd.read_csv("./raw/Nirone22.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Nirone22.columns[1:]]
Nirone22.columns = cols_x
Nirone22["instrument"] = "Nirone22"
Nirone22_final = pd.melt(Nirone22, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")

Nirscan_nano = pd.read_csv("./raw/Nirscan_nano.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Nirscan_nano.columns[1:]]
Nirscan_nano.columns = cols_x
Nirscan_nano["instrument"] = "Nirscan_nano"
Nirscan_nano_final = pd.melt(Nirscan_nano, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")

Scio = pd.read_csv("./raw/Scio.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Scio.columns[1:]]
Scio.columns = cols_x
Scio["instrument"] = "Scio"
Scio_final = pd.melt(Scio, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")

Tellspec = pd.read_csv("./raw/Tellspec.CSV", sep = ";")
cols_x = [ref_analyses.columns[0]] + [float(ii) for ii in Tellspec.columns[1:]]
Tellspec.columns = cols_x
Tellspec["instrument"] = "Tellspec"
Tellspec_final = pd.melt(Tellspec, id_vars = ["Sample","instrument"], var_name = "wavelength", value_name = "absorbance").merge(ref_analyses, on = "Sample", how = "left")

Tellspec_final


# In[55]:


# unifying

pd_final = pd.concat((F750_final,
Micronir1700rep1_final,
Micronir1700rep2_final,
Micronir1700rep3_final,
Micronir2200_final,
Nirone22_final,
Nirscan_nano_final,
Scio_final,
Tellspec_final))

pd_final.sort_values(["Sample","instrument","wavelength"]).to_csv(data_name + "_all.csv", sep = ";")
pd_final["instrument"].unique().tolist()


# In[56]:


# data documentation

data_doc = {}
data_doc["name"] = "Data set of Visible-Near Infrared handled and micro- spectrometers - comparison of their accuracy for predicting some sugarcane properties"
data_doc["description"] = "Chemical and Nir spectra measurements were made on 60 sugarcane samples from different plant parts (leaves, stem or whole aerial part). Chemical parameters (total sugar content - TS, crude protein content - CP, acid detergent fiber - ADF, in vitro organic matter digestibility - IVOMD were determined. In parallel, reflectance spectra were measured using eight spectrometers. To illustrate, PLS-R results are showed for the total sugar content and crude protein contents."
data_doc["source"] = "https://data.mendeley.com/datasets/rnvvftvmh7/1"
data_doc["chemical properties"] = pd_final.columns[-4:].tolist()
data_doc["instruments"] = pd_final["instrument"].unique().tolist()
data_doc["wavelenghts"] = "see column wavelength"


data_doc = pd.DataFrame.from_dict(data_doc, orient="index")
data_doc.columns = ["information"]
data_doc.to_csv(data_name + "_doc.csv", sep = ";")

