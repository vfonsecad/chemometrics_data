#!/usr/bin/env python
# coding: utf-8

# # Simulation of spectral data for NIR and calibration transfer using gaussian mixture loadings

# In[1]:



import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


# In[2]:


def genstruct(nstruct, nvar, ngauss, random_seed = 2394875):
    
    '''
    Function to generate structures such as loadings P using a mixture of gaussians
    
    Parameters:
    -----------
    
    nstruct: number of loading vectors
    nvar: number of spectral variables
    ngauss: number of gaussians for the mixture
    random_seed: int. Seed to use in the simulation
    
    Returns:
    --------
    
    structures: float array of shape (nstruct,nvar)
    
    '''

    structures = np.zeros((nstruct,nvar))

    z = np.arange(nvar)

    for a in range(nstruct):
        
        rng = np.random.RandomState(random_seed*a)
       
        x = rng.choice(nvar, ngauss, replace = True)    
        sd = rng.choice(np.arange(10,200), ngauss, replace = True)
        vec = np.zeros(nvar)

        for i in range(ngauss):

            gaussdens = (1/(np.sqrt(2*np.pi)*sd[i]))*np.exp(-(0.5/np.power(sd[i],2))*np.power(z-x[i],2))
            vec += gaussdens

        structures[a,:] = vec[:]
        
    structures_svd = np.linalg.svd(structures)
        
    return structures,structures_svd[2][0:nstruct,:]
    


# In[3]:


K = 1000 # number of wavelength channels
ncomp = 20
N = 600


# # Simulation

# In[4]:





# loadings and mean

Pt,Pt_svd = genstruct(nstruct = ncomp, nvar = K, ngauss = 4, random_seed = 7654)
x_mu = 50*genstruct(nstruct = 1, nvar = K, ngauss = 40)[0]

# scores

rng_scores = np.random.RandomState(4422455)
Tscores = rng_scores.multivariate_normal(mean = np.zeros(ncomp), cov = np.diag(np.linspace(50,10,ncomp)), size=N)
E = rng_scores.multivariate_normal(mean = np.zeros(K), cov = 0.0000005*np.eye(K), size=N)
X = Tscores.dot(Pt) + E + x_mu

# PCA

Xc = X - X.mean(axis=0)
cov_X = (Xc.T.dot(Xc))/N

svd_cov_X = np.linalg.svd(cov_X)

Pt_pca = svd_cov_X[0][:,0:ncomp] # loadings according to PCA, not simulated
Tscores_pca = Xc.dot(Pt_pca)

# simulation of y values


Tu = Tscores_pca[:,10:15] # chosing later components 
Q = np.linspace(3,0.1,Tu.shape[1])
beta_sim = 10
rng_y = np.random.RandomState(8765)


Y_clean = beta_sim + Tu.dot(Q) 
Y = Y_clean + rng_y.normal(loc=0,scale = 0.5, size = Tu.shape[0])


plt.scatter(Y_clean, Y)
plt.show()


# # PLS model building

# In[5]:


from sklearn.cross_decomposition import PLSRegression

xcal_pls = X.copy()
ycal_pls = Y.copy()

# choose model

chosen_lv = 10

my_pls = PLSRegression(n_components = chosen_lv,scale=False)
my_pls.fit(xcal_pls, ycal_pls)
x_mean = xcal_pls.mean(axis=0)
y_mean = ycal_pls.mean(axis=0)

# deliver model

B = my_pls.coef_
beta = y_mean - (x_mean.dot(B))

plt.plot(B)
plt.grid()
plt.show()


# In[6]:



N2 = 200


# scores

rng_scores = np.random.RandomState(87650)
Tscores2 = rng_scores.multivariate_normal(mean = np.zeros(ncomp), cov = np.diag(np.linspace(50,10,ncomp)), size=N2)
E2 = rng_scores.multivariate_normal(mean = np.zeros(K), cov = 0.0000005*np.eye(K), size=N2)
X2 = Tscores2.dot(Pt) + E2 + x_mu


# simulation of X2 with the same coefficients as calibrated model

# Y2_clean = X2.dot(B) + beta
# Y2_clean = Y2_clean.flatten()
# Y2 = Y2_clean + rng_y.normal(loc=0,scale = 0.5, size = X2.shape[0])


# PCA

X2c = X2 - x_mu
Tscores2_pca = X2c.dot(Pt_pca)

# simulation of y values


Tu2 = Tscores2_pca[:,10:15] # chosing later components 
rng_y = np.random.RandomState(3456)


Y2_clean = beta_sim + Tu2.dot(Q) 
Y2 = Y2_clean + rng_y.normal(loc=0,scale = 0.5, size = Tu2.shape[0])


plt.scatter(Y2_clean, Y2)
plt.show()


# In[7]:


# use model

xtest_pls = X2.copy()
ytest_pls = Y2.copy()

from sklearn.metrics import mean_squared_error, r2_score


ycal_pls_pred = xcal_pls.dot(B) + beta
rmsec = np.sqrt(mean_squared_error(ycal_pls, ycal_pls_pred))
r2c = r2_score(ycal_pls, ycal_pls_pred)


ytest_pls_pred = xtest_pls.dot(B) + beta
rmsep = np.sqrt(mean_squared_error(ytest_pls, ytest_pls_pred))
r2p = r2_score(ytest_pls, ytest_pls_pred)


# model performance

cal_title = "(cal performance): {:d} lv - rmsec: {:.2f} - r2c: {:.2f}".format(chosen_lv, rmsec, r2c) 

plt.subplots(figsize = (8,6))
plt.plot(ycal_pls,ycal_pls_pred,'o')
plt.plot([np.amin(ycal_pls),np.amax(ycal_pls)],[np.amin(ycal_pls),np.amax(ycal_pls)])
plt.grid()
plt.xlabel("y observed")
plt.ylabel("y predicted")
plt.title(cal_title) 
plt.show()

test_title = "(test performance): {:d} lv - rmsec: {:.2f} - r2c: {:.2f}".format(chosen_lv, rmsep, r2p) 

plt.subplots(figsize = (8,6))
plt.plot(ytest_pls,ytest_pls_pred,'o')
plt.plot([np.amin(ytest_pls),np.amax(ytest_pls)],[np.amin(ytest_pls),np.amax(ytest_pls)])
plt.grid()
plt.xlabel("y observed")
plt.ylabel("y predicted")
plt.title(test_title)
plt.show()


# # Save simulated data

# In[8]:


plt.plot(X.T, c = "blue")
plt.plot(X2.T, c = "orange")
plt.show()



# In[9]:


X_df = pd.DataFrame(X)
X_df["Y"] = Y
X_df["Type"] = "calibration"
X_df

X2_df = pd.DataFrame(X2)
X2_df["Y"] = Y2
X2_df["Type"] = "test"
X2_df


simdata = pd.concat((X_df,X2_df), axis = 0)


# In[10]:


simdata


# In[11]:


# unify

data_name = "simulation1"
simdata.to_csv(data_name + "_all.csv", sep = ";")


# In[12]:


# data documentation

data_doc = {}
data_doc["name"] = "Simulation NIR spectra"
data_doc["description"] = "Calibration set: 600 samples, Tst set: 200 samples"
data_doc["source"] = "https://doi.org/10.1016/j.chemolab.2020.103979"
data_doc["author"] = "Valeria Fonseca Diaz"
data_doc["chemical properties"] = "Y"
data_doc["data type"] = ["Calibration", "Test"]
data_doc["wavelenghts"] = "0-1000"


data_doc = pd.DataFrame.from_dict(data_doc, orient="index")
data_doc.columns = ["information"]
data_doc.to_csv(data_name + "_doc.csv")


# In[ ]:




