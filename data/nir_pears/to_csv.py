#!/usr/bin/env python
# coding: utf-8

# In[1]:


import scipy.io as sp_io
import numpy as np
import pandas as pd
import os

data_name = "pears"
# get raw data at https://chemom2021.sciencesconf.org/resource/page/id/5
# all files
files = []
for file in os.walk("./raw/"):
    files.append(file)

files = sorted(files[0][2])
files


# In[2]:


# get data (slow)
cal_df = pd.read_excel("./raw/Data_challenge_e_CHIMIOMETRIE_2021.xlsx", sheet_name = "CALSET", skiprows=2)


# In[3]:


wv_range = list(range(1,257))
cal_df.columns = ["instrument","sample_id","brix"] + wv_range
cal_df["instrument"].unique().tolist()


# In[4]:


# --- get transfer data

xtransfer_df = pd.read_excel("./raw/Data_challenge_e_CHIMIOMETRIE_2021.xlsx", sheet_name = 1, skiprows=2)
xtransfer_df.columns = ["instrument","sample_id"] + wv_range
xtransfer_df = xtransfer_df[xtransfer_df.iloc[:,0].isin(["Instr_1","Instr_2","Instr_3","Instr_4","Instr_5","Instr_6","Instr_7"])]
xtransfer_df["brix"] = None
transfer_df = xtransfer_df[["instrument","sample_id","brix"] + wv_range]


# In[5]:


transfer_df["instrument"].unique().tolist()


# In[6]:


# get test (val) data

xtest_df = pd.read_excel("./raw/Data_challenge_e_CHIMIOMETRIE_2021.xlsx", sheet_name = "VALSET", skiprows=2)
ytest_df = pd.read_excel("./raw/Challenge2021 Ytest.xlsx", sheet_name = "Sheet1", skiprows=0)


# In[7]:


ytest_df["Sample name"] = "Val" + ytest_df.loc[:,"Position"].astype(np.str)
test_df0 = pd.merge(xtest_df, ytest_df.loc[:,["Sample name", "Lab1"]], on = "Sample name", how = "left")
test_df0.columns = ["instrument","sample_id"] + wv_range + ["brix"]
test_df = test_df0[["instrument","sample_id","brix"] + wv_range]
test_df["instrument"].unique().tolist()


# In[8]:


# unify

pd_final = pd.concat((cal_df,transfer_df,test_df))
pd_final.to_csv(data_name + "_all.csv", sep = ";")


# In[9]:


# data documentation

data_doc = {}
data_doc["name"] = "Pear spectra 300 – 1140 nm portable instruments"
data_doc["description"] = "Cal SET : 1066 spectra Instr1  with Brix Std SETs : Inst 2 – 7  “slave”instrument  	        245 spectra same fruits  Test set : 993 spectra from the 7 instruments ( instrument is known) "
data_doc["source"] = "https://chemom2021.sciencesconf.org/resource/page/id/5"
data_doc["author"] = "Simon Kollaart  Agroscope Müller-Thurgau-Strasse 29 8820 Wädenswil Switzerland Phone +41 58 460 64 66 simon.kollaart@agroscope.admin.ch"
data_doc["chemical properties"] = "brix"
data_doc["instruments"] = pd_final["instrument"].unique().tolist()
data_doc["data type"] = ["Cal", "Std","Val"]
data_doc["wavelenghts"] = wv_range


data_doc = pd.DataFrame.from_dict(data_doc, orient="index")
data_doc.columns = ["information"]
data_doc.to_csv(data_name + "_doc.csv", sep = ";")


# In[10]:


pd_final["instrument"].unique().tolist()

