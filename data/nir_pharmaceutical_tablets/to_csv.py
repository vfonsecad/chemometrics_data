#!/usr/bin/env python
# coding: utf-8

# In[146]:


import scipy.io as sp_io
import numpy as np
import pandas as pd

# get raw data at https://eigenvector.com/resources/data-sets/
all_data = sp_io.loadmat("./raw/nir_shootout_2002.mat",struct_as_record = False)
all_data.keys()

data_name = "nir_shootout_2002"


# In[149]:


k = np.asarray(all_data["calibrate_1"])[0,0].data.shape[1] # no more information about the wv range
wv_range = list(range(1, k+1))


# In[150]:


# prepare data frame with all info

# cal data

pd_xcal1 = pd.DataFrame(np.asarray(all_data["calibrate_1"])[0,0].data.astype(np.float))
pd_xcal1.columns = wv_range
pd_xcal1["instrument"] = "instrument_1"
pd_xcal1["type"] = "calibration"

cal_id = ["sample_cal_" + str(ii) for ii in range(pd_xcal1.shape[0])]
pd_xcal1["sample_id"] = cal_id

pd_ycal = pd.DataFrame(np.asarray(all_data["calibrate_Y"])[0,0].data.astype(np.float))
pd_ycal.columns = np.asarray(all_data["calibrate_Y"])[0,0].label[1,0]
pd_ycal["sample_id"] = cal_id


pd_xcal2 = pd.DataFrame(np.asarray(all_data["calibrate_2"])[0,0].data.astype(np.float))
pd_xcal2.columns = wv_range
pd_xcal2["instrument"] = "instrument_2"
pd_xcal2["type"] = "calibration"

pd_xcal2["sample_id"] = cal_id

pd_cal = pd.concat((pd_xcal1.merge(pd_ycal, on = "sample_id", how = "left"),pd_xcal2.merge(pd_ycal, on = "sample_id", how = "left")))

# val data

pd_xval1 = pd.DataFrame(np.asarray(all_data["validate_1"])[0,0].data.astype(np.float))
pd_xval1.columns = wv_range
pd_xval1["instrument"] = "instrument_1"
pd_xval1["type"] = "validation"

val_id = ["sample_val_" + str(ii) for ii in range(pd_xval1.shape[0])]
pd_xval1["sample_id"] = val_id

pd_yval = pd.DataFrame(np.asarray(all_data["validate_Y"])[0,0].data.astype(np.float))
pd_yval.columns = np.asarray(all_data["validate_Y"])[0,0].label[1,0]
pd_yval["sample_id"] = val_id

pd_xval2 = pd.DataFrame(np.asarray(all_data["validate_2"])[0,0].data.astype(np.float))
pd_xval2.columns = wv_range
pd_xval2["instrument"] = "instrument_2"
pd_xval2["type"] = "validation"
pd_xval2["sample_id"] = val_id



pd_val = pd.concat((pd_xval1.merge(pd_yval, on = "sample_id", how = "left"),pd_xval2.merge(pd_yval, on = "sample_id", how = "left")))


# test data

pd_xtest1 = pd.DataFrame(np.asarray(all_data["test_1"])[0,0].data.astype(np.float))
pd_xtest1.columns = wv_range
pd_xtest1["instrument"] = "instrument_1"
pd_xtest1["type"] = "test"

test_id = ["sample_test_" + str(ii) for ii in range(pd_xtest1.shape[0])]
pd_xtest1["sample_id"] = test_id

pd_ytest = pd.DataFrame(np.asarray(all_data["test_Y"])[0,0].data.astype(np.float))
pd_ytest.columns = np.asarray(all_data["test_Y"])[0,0].label[1,0]
pd_ytest["sample_id"] = test_id

pd_xtest2 = pd.DataFrame(np.asarray(all_data["test_2"])[0,0].data.astype(np.float))
pd_xtest2.columns = wv_range
pd_xtest2["instrument"] = "instrument_2"
pd_xtest2["type"] = "test"
pd_xtest2["sample_id"] = test_id


pd_test = pd.concat((pd_xtest1.merge(pd_ytest, on = "sample_id", how = "left"),pd_xtest2.merge(pd_ytest, on = "sample_id", how = "left")))

pd_test.shape


# In[156]:


# unify 


pd_final0 = pd.concat((pd_cal,pd_val,pd_test))
cols = pd_final0.columns.tolist()
pd_final = pd_final0[cols[-4:-7:-1] + cols[-3:] + cols[:-6]]

pd_final.to_csv(data_name + "_all.csv", sep = ";")


# In[152]:


# data documentation

data_doc = {}
data_doc["name"] = "nir spectra pharmaceutical tablets from shootout"
data_doc["description"] = "In 2002, the International Diffuse Reflectance Conference (IDRC) published a Shootout data set consisting of spectra from 654 pharmaceutical tablets from two spectrometers. The data is divided up into calibration, validation and test sets..."
data_doc["source"] = "https://eigenvector.com/resources/data-sets/"
data_doc["chemical properties"] = pd_final.columns[3:6].tolist()
data_doc["instruments"] = pd_final["instrument"].unique().tolist()
data_doc["data type"] = pd_final["type"].unique().tolist()
data_doc["wavelenghts"] = wv_range


data_doc = pd.DataFrame.from_dict(data_doc, orient="index")
data_doc.columns = ["information"]
data_doc.to_csv(data_name + "_doc.csv", sep = ";")

